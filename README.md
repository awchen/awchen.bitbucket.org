# README #

# TO-DO #

* Implement 2-block-wide grid in projects.html - YAZEEDZ
* Implement Ruby email protocol - AWCHEN/YAZEEDZ
* Implement Heroku URL rerouting - AWCHEN/YAZEEDZ
* Convert website to Ruby after first release - AWCHEN/YAZEEDZ
* Continued style implementation - AWCHEN
* MAYBE: Have navbar fade away after downscroll - AWCHEN


# FINISHED #

* Add proper style for <a> tags (cool green color) - AWCHEN
* Styles for buttons - AWCHEN
* Fixed margining issue in right margin - AWCHEN
* Fixed margining issue above text blocks - AWCHEN/YAZEEDZ
* Implement blocks in HTML - YAZEEDZ
* Implement a block background image and then implement parallax - YAZEEDZ
* Style implemented - style for #main-title completed - AWCHEN
* Footer - Changed to static fixed - AWCHEN
* Fix *#section2* not properly referencing to its .css config - YAZEEDZ
* Style implemented - style for #section2 completed - AWCHEN
* Switch to stellar - YAZEEDZ
* Vertically center #section2 and #nyancat (rename to #section3) - YAZEEDZ
* Implement a box in #section3 with a parallax background - YAZEEDZ
* Implemented form - AWCHEN
* Disabled overlap between footer and content - AWCHEN
* Style implemented - style for form completed - AWCHEN
* Added buttons for email, Github, and LinkedIn profiles on main page - AWCHEN
* Added scroll notifier + responsiveness - AWCHEN/YAZEEDZ
* Created <div> template - AWCHEN
* Implemented slider - YAZEEDZ
* Style implemented - style for form completed - AWCHEN

# CONTRIBUTORS #
* awchen
* yazeedz