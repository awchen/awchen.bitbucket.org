require 'rubygems'
require 'mail'

post '/email' do
  begin
    froms = params[:name] + ' <' + params[:email] + '>'
    subjs = params[:subject] || ''
    bodys = params[:message] || ''
    semail = "awc8888@gmail.com" || ''
    Mail.deliver do
      to semail
      from froms
      subject subjs
      body bodys
    end
    "success"
  rescue
    "error"
  end
end